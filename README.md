# JS PracticaFinal

## Descripcion

Implementar una página web que muestre
las películas listadas por la API
proporcionada por el sitio The Movie DB

--------
La idea principal era poder buscar una pelicula a partir de su nombre , o bien peliculas relacionadas con ese mismo nombre.


## Resolucion

Lo que se hizo fue tomar la idea que se nos presento de hacer el llamado a la api y apartir de la respuesta irla metiendo en una lista, cada elemento se iba a formatear con su imagen , su titulo y su descripcion.
Se toma en consideracion la implementacion de el catalogo para tener una mejor idea.


## Observaciones 

Algunas imagenes no se mostraron ya que al parecer no se encuntran en el servidor de esta api.
Tambien se tuvo algo de problemas al llamar a la api ya que el api-key no estaba bien , se procedio a usar el mismo que de catalogo para poder avanzar.

## Previo
![info mefud](https://media.discordapp.net/attachments/846165595710816287/1039066188401098793/image.png?width=963&height=357)

